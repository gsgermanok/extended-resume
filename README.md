# Resume Extended (in progress)

## About me

To whom it may concern,

Over 20 years of personal experience working in Software development, I work with languages from ASP to GoLang.

## Languages (description in progress)

### Python

I use python each day because is a powerful language for writing large and small applications for maintenance.

### Micropython

is the same as python but with fewer libraries included. It is a powerful language for creating IoT projects. I used this language on ESP82266 an ESP32 board.
> [micro mvc](https://gitlab.com/iot-micropython/micro-mvc)
- [x] [current project 1](images/iot-micropython-1.jpg) **Picture**
- [x] [current project 2](images/iot-micropython-2.jpg) **Picture**


### PHP

I writen in php from 2003 and is the mayorly language with I use in my work each day.

### Ruby



### Golang

### Java

I used java in Androd Studio.

### Flask

this framework is very useful when we need to write maintenance applications or services, we can use the power of python in a small web application.

> I wrote an application that report and blocks attacks in Cloudflare.

### Sinatra

### Laravel

Studio98 migrate his from scratch platform to Laravel. I worked in that migration process in database maintenance and writing feeds.


### Ruby on Rails

I learn many years Ruby and ROR. And use this language in decisiv company (trucks mainteinance application) personal projects.
the framework come with many featured and easy and intuitive way for do test rspec or minitest.

### Django

Is the preferred framework for my projects. I use generally django and postgres it is a great couple.

- [x] [tribu](https://tribu.site/)
- [x] [eos web](https://eosweb.info/)


### Android Studio

Write client for a IoT project.

> the application manage IP device from cell phone. [Device](https://www.telcodepot.com/mission-machines-ipdp1000-ip-door-phone)
- [x] [demo](https://www.youtube.com/watch?v=LnPWrQYh8EU) **Video-Youtube**

### c / c++

Arduino Ide 
- [x] [first arduino project](https://www.youtube.com/shorts/xBZktiCc-3k) **Video-Youtube**
- [x] [Examples of array and leds](https://www.youtube.com/shorts/lSvs8jVA59w) **Video-Youtube**

Language used at University

## Scripts

### bash

### javascript

## Technologies and tools (description in progress) 

### Vim

### Jupyter

### Docker

I use Docker in my work each day because enables the possibility of separating the configuration between the applications and the system.
> I have a presentation written by me in go-present where explain in Spanish the way to use docker and docker-compose used in meetups of ruby Santa Fe Argentina [Presentation](https://github.com/ggerman/pandora)

### AWS

S3
Lambda functions: I use them for massive images treatment with python.
> [Lambda Function](https://github.com/ggerman/lambda-aws)

### Git

github, gitlab

### Web Server: 

Apache, nginx

### Linux: 

Debian / Ubuntu


## Databases (description in progress)
> mysql
> postgres
> redis
> sqlite

## Education and updates (description in progress)

Universidad Autónoma de Entre Ríos - Bachelor in Systems (1996 - 2001)

> Cafe Conf:
> Cafe Conf:
> [PyConAr 2017](https://eventos.python.org.ar/events/pyconar2017/): (16 to 27 de November 2017) 
> [PyConAr 2018](https://eventos.python.org.ar/events/pyconar2018/): (22, 23, 24 November 2018)
> [PyConAr 2019](https://eventos.python.org.ar/events/pyconar2019/): (4, 5 and 6 December 2019)

## Books (description in progress)


## Work Experience Companies (description in progress) 

imagineretailer.com studio98: [urls](imagineretailer-urls.md)
> PHP, Python, Laravel, Apache, Docker, nginx, Shopify, letsentcrypt, AWS, Google Cloud, CloudFlare, SendGrid

decisiv.com
> Ruby on Rails, IIS, RSpec, Minitest

telcodepot.com
> Magento, PHP

NU-LIFE WORLDWIDE, LLC
> Magento, Wordpress, Drupal, Joomla, PHP

turismoentrerios.com: [urls](turismoentrerios-urls.md)
> PHP from scratch websites.
> Sysadmin, debian, apache, postfix, mysql, bind


IIIMPACT, Inc.
> Drupal, Wordpres, Php, Joomla

Creativeworks.com
> Magento, Drupal, Wordpres, Php, Joomla

UADER
> Sysadmin, documentation, and teaching project.

